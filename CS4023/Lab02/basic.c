//
// Created by User on 13/10/2022.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void ifelse();
void chars();
#include "utils.h"



int main(){
    char input;
    FILE *from;
    FILE *to;
    char c;
    scanf("%c", &input); //Making breaks in the program for presenting
    printf("Hello World!");
    scanf("%c", &input); //Making breaks in the program for presenting
    ifelse();
    scanf("%c", &input); //Making breaks in the program for presenting
    chars();
    chars();
    chars();
    chars();
    chars();
    chars();
    chars();
    chars();
    from = fopen("file.txt", "r");
    to = fopen("reversed.txt", "w");
    while ((c = getc(from)) != EOF){
        putc(flipCase(c), to);
    }
    fclose(from);
    fclose(to);
    exit(0);
}

void ifelse(){
    int true = strcmp("Hello", "Hello");
    int false = strcmp("Hello", "World");
    printf("strcmp returns %i if 2 strings are equal, while it returns %i if they aren't\n", true, false);
    printf("But C thinks: ");
    if(true){
        printf("Never run");
    } else {
        printf("%i is false while ", true);
    }
    if(false){
        printf("%i is true\n", false);
    } else {
        printf("never run");
    }
    printf("To combat this we need to use a simple equate operation to make it work intuitively\n");
    true = (true == 0);
    false = (false == 0);
    if(true){
        printf("True is now true\n");
    } else {
        printf("%i is false while ", true);
    }
    if(false){
        printf("%i is true\n", false);
    } else {
        printf("False is now false\n");
    }
}

void chars(){
    char c;
    printf("Please input a char: ");
    scanf("%c", &c);
    printf("%c is %i while working in C\n", c, c);
}
