# Important notices
All of these notes and example code was provided to me as part of the CE4703 module in which I am 
actually thought how to program in C. NONE OF THIS WORK (other than the README file) is mine, I 
am making these available to ye in good faith, please do not distribute these further than within this 
student group. All work was done by Dr Reiner Dojen. I will provide all Sample code that is provided.

# Provided chapters
1. Unit 1: Intro to C.

# How to compile and run the code
You can compile and run the code by running ``make [file_name]`` (file name is in camelcase) to compile the
programs, and you can run them with ``./[file_name]`` (file name is in the same format as the make command) 
and then to clear the compiled files just run ``make clear``.

# Apple macbooks
An issue has come up with the assignment when completed on Apple Macs that use Apple's M series processors, the issue is
detailed in CS4023/macs.md. If any more issues arise from Mac users, please raise an issue on the repository (or message 
me on discord: ``eoghanconlon73#5373``).

# ICTLC supports
The ICT Learning center run supports for C programming, below are the times in which those supports are run:

|Day      |Time         |Person         |Topics covered              |
|---------|-------------|---------------|----------------------------|
|Monday   |14:00 - 15:00|Luke O'Laughlin|Java/C++/C/Web/SQL/Github   |
|Tuesday  |10:00 - 11:00|Paulis Gributs |Java/C#/C++/C/Python/Android|
|Tuesday  |11:00 - 12:00|Jim Ryan       |Java/C/C++/Python/Web       |
|Tuesday  |16:00 - 17:00|Andrei Smirnou |Java/C++/C/Python/SQL/Web   |
|Wednesday|10:00 - 11:00|Clodagh Walsh  |Java/C++/C/Python/PHP/Web   |
|Wednesday|11:00 - 12:00|Jim Ryan       |Java/C/C++/Python/Web       |
|Wednesday|14:00 - 15:00|Luke O'Laughlin|Java/C++/C/Web/SQL/Github   |
|Thursday |12:00 - 13:00|Josh McGiff    |Java/C/C++/                 |

# Textbooks suggested by my lecturer
* C How to Program, Deitel, P., Deitel, H., Pearson
* Applied C: An Introduction and More, A.E. Fischer, D.W. Eggert, S.M. Ross. McGraw-Hill.
* The Joy of C, L.H. Miller, A.E. Quilici. McGraw-Hill.
* Programming in C, Kernighan, Ritchie, Prentice Hall.
* Algorithm in C, R. Sedgewick. Addison-Wesley.
* Data Structure and Algorithms, A.V. Aho, J.E. Hopcroft, J.D. Ullman. Addison Wesley.
* Introduction to Algorithms, T. Cormen, C. Leiserson, R. Rivest. MIT Press.