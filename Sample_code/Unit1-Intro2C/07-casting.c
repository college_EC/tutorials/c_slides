// casting.c
//
// sample file to demonstrate use of implicit
// and explicit casting in c
//
// author: reiner dojen
// date: 10.09.2017

#include <stdlib.h>
#include <stdio.h>

int main() 
{
  double d=3.8;   // a variable with fractional part
  int i=2 ;       // a whole number variable

  int iResult=0;    // integer result
  double dResult=0; // floating point result

  // here i is upcasted to double,
  // as dResult is also a double all is fine
  dResult = d * i;
  printf("%f * %d = %f\n", d, i, dResult);
  
  // here i is upcasted to double,
  // as iResult is an integer, the result of
  // the calculation is truncated to integer
  iResult = d * i;
  printf("%f * %d = %d\n", d, i, iResult);
  
  // same with explicit cast (c style)
  // note the brackets around d * i, without them
  // you would get ((int) d) * i; // = 3*2 = 6 
  iResult = (int) (d * i);
  printf("%f * %d = %d\n", d, i, iResult);

  // dummy assignment to suppress warning about unsued variables
  dResult = iResult;
  return EXIT_SUCCESS;
}
  
