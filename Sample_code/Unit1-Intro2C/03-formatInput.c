/*********************************\
| formatInput.c                             |
| sample program to demonstrate|
| console input in C                      |
|                                                  |
| author: Reiner Dojen                 |
| date: 9.9.2015                           |
\*********************************/

#include <stdlib.h>
#include <stdio.h>

int main()
{
  int i;    // integer variable
  long long ll; // very large integer
  float f;  // single precision floating point
  double d; // double precision floating point

  // read in some values
  // try something large like 1234567890987654321
  // always prompt user for what is expected !!!

  printf("Please enter an integer value: ");
  scanf("%i", &i); // don't forget the '&'
  printf("You entered the value %d\n", i);

  printf("Please enter a very large integer value(19 digit): ");
  scanf("%i", &i); // don't forget the '&'
  printf("You entered the value %d\n", i);

  printf("Please enter a very large integer value(19 digit): ");
  scanf("%lld", &ll); // don't forget the '&'
  printf("You entered the value %lld\n", ll);

  printf("Please enter a float value: ");
  scanf("%f", &f); // don't forget the '&'
  printf("You entered the value %f\n", f);

  printf("Please enter a double value: ");
  scanf("%lf", &d); // don't forget the '&'
  printf("You entered the value %f\n", d);

  // using incorrect target type will read incorrect value
  printf("Please enter a double value: ");
  scanf("%lf", &f); // don't forget the '&'
  printf("You entered the value %f\n", f);
  

    return EXIT_SUCCESS;
}

