/*
 * helloWorld.c
 * sipmle hello world program to demonstrate basic C
 * features.
 *
 * author: reiner dojen
 * date: 1.10.2002
 */

// #include <...> provides standard/system libraries
#include <stdlib.h>
#include <stdio.h>

// main is the starting point for every application  
int main()
{
  // print string "Hello Worldshffskdhf!!!"
  printf("Hello Worldsdfgsdfgsdf !!!\n");

  // return to OS and report success
  return EXIT_SUCCESS;
  // alternative: return 0;
}
