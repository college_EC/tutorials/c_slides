# Lab 1
I will be ignoring the ``#import`` lines at the start of the program as they are just the standard packages that are 
imported. If he imports some package that adds major functionality to the program I will explain it then.
## Code
Below is the full code as provided by Sahir: <br />
```C
#import <stdio.h>
#import <stdlib.h>

//Start of program
int main(){
  //Variable declaration and assignment (with error catching)
  char c;
  FILE *from;

  from = fopen("file.txt", "r");
  if (from == NULL){
    perror("file.txt");
    exit(1);
  }
  
  //Main part of the program
  /* file exists, so start reading */
  while ((c = getc(from)) != EOF){
    putc(c, stdout);
  }
  
  //End of the file
  fclose(from);

  exit(0); //return 0;
}
//I have made a few changes to the code, but they
//are only asthetic changes for readability, such as
//moving the curly brackets to the same line as the
//if statement or the int main() line and I also
//braced the while loop in curly brackets despite
//that not being necessary.
```
I will be taking the code in segments which are laid out by comments above those lines of code.

## Code explained
### Start of program
```C
int main() {
```
You may have noticed that this line has some similarities to the start of the main execution block in Java,
```Java
public static void main(String args[]){
```
Both are main functions which means that these are executed when you run the program. The main difference between Java and
C is that in C the main function returns an integer value instead of nothing as it does in Java.

### Variable declaration and assignment
For this explination I will break the segment into 3 different segments.
```C
//Variable declaration and assignment
char c; //signed char c;
FILE *from;

from = fopen("file.txt", "w");
//Error Handling
if (from == NULL){
    perror("file.txt");
    exit(1); //return 1;
}
```
#### Variable declaration and assignment
In C it is good practice to declare any variable used at the start of the program (the for loop is an exception!!). In 
this file we use 2 main variables a character[1] and a pointer to a file. <br />
The character variable is used later, but we assign a value to the pointer immediately and we assign it to the file called
file.txt[2]

#### Error Handling
Sometimes the system can't find the file or maybe it can find the file but the current user doesn't have read permissions
the ``fopen`` function returns NULL, instead of the pointer to the file which would cause the program to crash later on, 
so to mitigate this we have this if statement that prints out the error that it runs into and then exits the program with 
a non-zero value, signifying to the computer that something went wrong.

[1] There is a known issue with the character variable in this program when compiled and run on an Apple Mac with an M 
series chip, for more information please see macs.md <br />
[2] ``fopen`` is a very interesting function, it opens a file in one of 6 modes, but I will only go through 3 of them here. 
There is the read mode (``"r"``) that is used in this program, in which the program can only read the contents of the 
specified file, there is the write function (``"w"``) that is useful for our assignment as it opens a new file and that 
it can write to, though it does have a shortfall that if there is already a file of that name there, it will override it, 
the last one is the append mode (``"a"``), this is similar to the write mode but the program opens an already existing file,
and it appends the output to it {not really necessary but good for completions sake}

### Main part of the program
```C
while ((c = getc(from)) != EOF){
    putc(c, stdout);
}
```
This part of the program is where the printing to console happens. What it does is that it gets the first character of the
variable from (in this case the file file.txt) and assigns it to c, then it checks if that equals EOF, so for example, the 
contents of my file.txt was:
```txt
Hello World!
```
it would get the first character (H) check to see if that is the same as EOF (which it isn't), and then proceeds into the
main body of the loop. <br />
Here we see the ``putc`` function, this function places a character onto somewhere sprcified, in the case of the program 
it puts the character stored in variable c onto the standard output (normally the terminal), to continue with my example 
above, c contains the value ``'H'``, so this program would place this onto the terminal. <br />
The program then repeats this proccess for all characters in from until it reaches the EOF character and terminates the loop.

### End of file
```C
fclose(from);
exit(0);
```
A thing to note about C is that it is a non-garbage collected language which means that it is on the programmer to close 
out the variables, small variables such as numbers or letters are fine but for bigger variables such as files, it is very 
important to close them when you are finished with them. And finally we reach the last line of the program, this exits the 
program returning the integer 0, it is good practice to do this as this tells the computer that the proccess completed 
successfully. 

<br />
<br />
I hope that this was of some benefit to you, in understanding the program that Sahir provided. - Eoghan