// Preincrementing and postincrementing
// Deitel & Deitel, C How to Program
//
// 12/09/2018

#include <stdlib.h>
#include <stdio.h>

// function main begins program execution
int main()
{
  int c, a; // define variable

  // demonstrate postincrement
  c = 5; // assign 5 to c
  printf ("%d\n", c); // print 5
  printf ("%d\n", c++); // print 5, then postincrement
  printf ("%d\n", c); // print 6

  printf("Please press enter to key to continue ....\n");
  getchar();
  
  // demonstrate preincrement
  c = 5; // assign 5 to c
  printf ("%d\n", c); // print 5
  printf ("%d\n", ++c); // preincrement then print 6 
  printf ("%d\n", c); // print 6

  //Always true as 5 is true.
  if (a=5) {
    printf("a is five");
  }

  //This is always false as a = 5 which does not equal to 4
  if (a==4) {
    printf("I never print");
  }

  return EXIT_SUCCESS;
}
