////////////////////////////////////////////////////
// storageClassSampleMain.c
//
// sample file to demonstrate use of different
// storage classes.
//
// author:	reiner dojen
// date:	05.10.2006
////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>

// declare functions
void countAuto();
void countRegister();
void countStatic();

void changeProjectGlobal();

// use an extern variable - this is a "pure"
// declaration, the variable must be defined
// somewhere else
extern int projectGlobal;


int main() 
{
  int i;
  
  // call count auto - variable 
  // behaves "normally"
  printf("Calling countAuto() 5 times\n");
  for (i=0; i<5; i++)  countAuto();

  printf("Please press enter to key to continue ....\n");
  getchar();

  // call count register - variable 
  // behaves "normally"
  printf("Calling countRegister() 5 times\n");
  for (i=0; i<5; i++)  countRegister();

  printf("Please press enter to key to continue ....\n");
  getchar();

  // call count static - variable 
  // keeps value between calls
  printf("Calling countStatic() 5 times\n");
  for (i=0; i<5; i++)  countStatic();

  printf("Please press enter to key to continue ....\n");
  getchar();
  
  // work with external variable
  // use as "normal" variable
  // function changeProjectGlobal can 
  // change value !!!
  printf("projectGlobal = %d\n", projectGlobal);
  printf("call changeProjectGlobal\n");
  changeProjectGlobal();
  printf("now projectGlobal = %d\n", projectGlobal);
  
  return EXIT_SUCCESS;
}

////////////////////////////////////////////////////
// countAuto()
// demonstrate the loss of information of auto
// variables
////////////////////////////////////////////////////
void countAuto() 
{
  int counter = 0; // auto variable (default storage class)
  counter++;
  printf("counter = %d\n",counter);
  return;
}




















////////////////////////////////////////////////////
// countRegister()
// demonstrate the loss of information of register
// variables
////////////////////////////////////////////////////
void countRegister() 
{
  register int counter = 0;
  counter++;
  printf("counter = %d\n",counter);
  return;
}













////////////////////////////////////////////////////
// countStatic()
// demonstrate the loss of information of register
// variables
////////////////////////////////////////////////////
void countStatic() 
{
  static int counter = 0;
  counter++;
  printf("counter = %d\n",counter);
  return;
}

