/*
 * GeometricSeries.c
 *
 * prints out the first 5 elements of the geometric 
 * series 1/2 + 1/4 + 1/8 + 1/16 ...
 *
 * highlights the importance of proper type selection of 
 * constants and variables
 */

#include <stdlib.h>
#include <stdio.h>

void printGeometricSeries(int termCount);

int main() 
{
  // ad hoc attempt to create terms
  printf("ad hoc attempt, using constants:\n "
	 "first term: %f\n"
	 "second term: %f\n"
	 "third term: %f\n"
	 "fourth term: %f\n"
	 "fifth term: %f\n",
	 1/2, 1/4, 1/8, 1/16, 1/32);
  
  printf("Please press enter to continue:\n");
  getchar();

  





  
  
  // attempt with variables:
  int denom=2;
  printf("\n\n\nusing a for loop with integers:\n");
  for (int i=0; i<5; ++i) {
    printf("%f\n", 1/denom);
    denom *= 2;
  }










  


  int terms;
  // read in desired number of terms
  printf("\n\n\nproper attempt with correct types\n");
  printf("How many terms do you want to calculate? ");
  scanf("%i", &terms);
  double denominator=2;
  for (int i=0; i<terms; ++i) {
    printf("%f\n", 1.0/denominator);
    denominator *= 2.0;
  }
  
  return EXIT_SUCCESS;
}

