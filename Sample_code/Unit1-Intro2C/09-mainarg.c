/*****************************************************
| mainarg.c					                        
| example file for argument passing from command line 
| to main function.                                   
|                                                     
| Author: Reiner Dojen                                
| Last Change:	01/10/2002                            
\*****************************************************/

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) 
{
  
  int i;
  double x;
  
  // test for correct number of arguments
  if (argc < 3) {
    // wrong number of arguments, print error message
    printf("Wrong number of arguments\n");
    printf("Please call with:\n\n");
    printf("\tmainarg integer double\n\n");
    
  } else {
    // everything is fine
    // echo all parameter to screen
    for (i=0; i<argc; i++)
      printf("Argument %d (argv[%d]) = \"%s\"\n",i+1 ,i ,argv[i]);
    
    
    // transform first argument into int
    // and second into double (neglecting name at argv[0])
    i = atoi(argv[1]);
    x = atof(argv[2]);
    
    printf("\n\nString \"%s\" = %d\n", argv[1], i);
    printf("String \"%s\" = %f\n\n", argv[2], x);
  }
  return EXIT_SUCCESS;
}
