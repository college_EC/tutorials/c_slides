//
// Created by User on 13/10/2022.
//

char flipCase(char c){
    if('a'<= c && 'z' >= c){
        return c - 'a' + 'A'; //returns c as a capital
    }
    if('A' <= c && 'Z' >= c){
        return c - 'A' + 'a'; //returns c as lowercase
    }
    return c; //c is neither an uppercase letter nor a lower case one, so it is ignored
}
