//////////////////////////////////////////////////////
// BitwiseOperators.c
//
// demonstration of use of bitwise operators
// in the C language
//
// Author:	Reiner Dojen
// Date:	29.09.2005
//////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>

void printBits(int number, int digits);

int main() 
{
  
  int a, b; // variables for values
  int bitmask; // variable for bit-mask
  int result; // variable for some operation results
  
  
  // use binary AND to test for set bits
  
  // set value for a and b;
  a = 42;
  b = 46;

  // test for bit 3 (2^2 = 4)
  // set mask to bit 3
  bitmask = 0x04;

  printf("\n\na = %d = ", a);
  printBits(a, 8);
  printf("\nb = %d = ", b);
  printBits(b, 8);
  printf("\n");
  
  // test values for a
  result = a & bitmask;
  printf("\n\n\t ");
  printBits(a, 8);
  printf(" (a)\n\t&");
  printBits(bitmask, 8);
  printf(" (bitmask)\n\t --------\n\t ");
  printBits(result, 8);
  
  if ( (result) == 4 ) 
    printf("\na (%d) has bit 3 set to 1\n", a);
  else
    printf("\na (%d) has bit 3 set to 0\n", a);

  printf("Please press enter to key to continue ....\n");
  getchar();
  
  // test values for b
  result = b & bitmask;

  printf("\n\n\t ");
  printBits(b, 8);
  printf(" (b)\n\t&");
  printBits(bitmask, 8);
  printf(" (bitmask)\n\t --------\n\t ");
  printBits(result, 8);
  if ( (result) == 4 )
    printf("\nb (%d) has bit 3 set to 1\n\n\n", b);
  else
    printf("\nb (%d) has bit 3 set to 0\n\n\n", b);

  printf("Please press enter to key to continue ....\n");
  getchar();
  
  // use bitwise AND to clear bits
  a = 0xaa;		// = 1010 1010
  bitmask = 0xf0;	// = 1111 0000
  
  // clear bits in a
  result = a & bitmask;
  
  printf("clear bits in value\n\t ");
  printBits(a, 8);
  printf(" (value)\n\t&");
  printBits(bitmask, 8);
  printf(" (bitmask)\n\t --------\n\t ");
  printBits(result, 8);
  printf("\n\n\n");

  printf("Please press enter to key to continue ....\n");
  getchar();

  
  // use bitwise OR to set bits
  a = 0xaa;		// = 1010 1010
  bitmask = 0xf0;	// = 1111 0000
  
  // set bits in a
  result = a | bitmask;
  printf("set bits in value\n\t ");
  printBits(a, 8);
  printf(" (value)\n\t|");
  printBits(bitmask, 8);
  printf(" (bitmask) \n\t --------\n\t ");
  printBits(result, 8);

  printf("\nPlease press enter to key to continue ....\n");
  getchar();

  
  printf("\n\nOne's complement 'inverts' all bits:\n");
  printf(" a =\t");
  printBits(a,8);
  printf("\n~a =\t");
  printBits(~a,8);
  printf("\n");

  return EXIT_SUCCESS;
  
}

//////////////////////////////////////////////////////
// printBits(number, digits)
// prints a binary number to screen
// prints exactly digits bits (max 32)
// msb is leftmost bit, lsb is rightmost bit
//
// param: number - number to be printed
//	  digits - number of bits to be printed
//	    	   (counted from lsb)
// return: void
//////////////////////////////////////////////////////
void printBits(int number, int digits) {
  int bitmask;	// variable for bitmask
  int bit;	// variable for current bit-value
  
  // restrict digits to max 32 bits
  if (digits > 32) digits = 32;
  bitmask = 0x01 << (digits-1);
  
  // tick of bits, starting from leftmost bit (msb)
  while (bitmask != 0) {
    if ( (number & bitmask) != 0) bit = 1;
    else bit = 0;
    printf("%d", bit);
    // shift bitmask one bit to right to get next
    // bit (in direction of lsb)
    bitmask >>= 1;
  }
}

