/***********************************************\
| funArg.c                                      
| example file for parameter passing to and from
| functions.                                    
|                                               
| Author:Reiner Dojen                           
| Last Change:01/10/2002                        
\***********************************************/

#include <stdio.h>
#include <stdlib.h>

// function declarations
double dmax(double x, double y);
void f(int i, double e, char c, float f, double g);


int main() 
{
  double x,y,z; // variables for calculation

  // assign values to variables
  x = 5.0;
  y = 7.0;
  z = 10.0;
  
  // call function f with various parameters
  // parametere are replaced with values (e.g. 
  // expressions), not statements/declarations
  f(42, 3.141, 'A', 3.11f, 2.718);
  
  // use literals, variables and function call
  f((int) x,  y, 'B', dmax(x,y), dmax(y,z));
  
  // use variables and function return 
  // value in expression
  z = y + 3.324 + dmax(x,y) / x;
  
  // use nested function call as parameter
  printf("\nMax value of %.3f, %.3f and %.3f is %.3f\n",
	 x, y, z, dmax(z,dmax(x,y)));
  
  return EXIT_SUCCESS;
}

/**********************************\
| double dmax(double x, double y)  |
| returns max value of x and y     |
\**********************************/
double dmax(double x, double y) {
  return x>y?x:y;
}

/*****************************************************\
| void f(int i, double x, char c, float f, double z)  |
| just a dummy function with many parameters and no   |
| return type showing that all types can be used as   |
| as parameter                                        |
\*****************************************************/
void f(int i, double e, char c, float f, double g ) {
  printf("\nCalled function f with values i=%i, e=%.3f, "
	 "c=%c, f=%.3f, g=%.3f\n", i,e,c,f,g);
  return;
}
