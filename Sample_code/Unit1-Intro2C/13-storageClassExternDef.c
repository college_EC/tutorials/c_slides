////////////////////////////////////////////////////
// storageClassExternDef.c
// defines external variable and function for
// storageClassMain.c
//
// author:	reiner dojen
// date:	05.10.2006
////////////////////////////////////////////////////

// declare functions
void changeProjectGlobal();

// define global variable
int projectGlobal = 1;

////////////////////////////////////////////////////
// changeProjectGlobal()
// multiplies the global variable by 10;
////////////////////////////////////////////////////
void changeProjectGlobal() {
  // mulitply projectGlobal by 10
  projectGlobal *= 10;
  return;
}
