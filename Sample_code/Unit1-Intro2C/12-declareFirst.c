/*
 * declareFirst.c
 * declare all functions before calling them
 * define them later in the file
 *
 * Author: Reiner Dojen
 * Last Change:	01/10/2002
 *
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * declare all functions
 *
 */
double dmax(double x, double y);
double dmin(double x, double y);


/*
 * now dmin and dmax can be used in main
 * even though functions are not defined yet
 * declaration tells compiler these funcions
 * will be defined somewhere in file
 *
 */
int main(int argc, char *argv[]) 
{
  double var1=4.234;	// first value
  double var2=5.23;	// second value
  double max, min;	// variable for max and min

  // we now can use functions min/max although
  // they are not defined yet :-)
  max = dmax(var1, var2);
  min = dmin(var1, var2);

  printf("min = %f, max = %f", min, max);

  return EXIT_SUCCESS;
}

/*
 * double dmax(double x, double y)
 * returns max value of x and y
 *
 */
double dmax(double x, double y) 
{
  // (expr1)?(expr2):(expr3)
  return (x>y)?(x):(y);
}

/*
 * double dmin(double x, double y)
 * returns min value of x and y
 *
 */
double dmin(double x, double y) 
{
  return x>y?y:x;
}
