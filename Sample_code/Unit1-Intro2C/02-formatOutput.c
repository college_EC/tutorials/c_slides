//////////////////////////////////////////
// formatOutput.c
// demonstrates some features of 
// C console output
//
// author: Reiner Dojen
// date: 9.9.2015
//////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>

int main() 
{
  int i = 1234; // integer variable
  char *s = "Hello !!!"; // constant string
  double x = 12.123456789; // double variable

  // sample basic output
  printf("Sample output for printf:\n");
  printf("s is \"%s\", i is %d and x is %f\n", s, i, x);

  // using min field width
  printf("\nUsing min field width:\n");
  printf("s is \"%20s\", i is %10d and x is %10f\n", s, i, x);

  // using left alignment
  printf("\nUsing left alignment:\n");
  printf("s is \"%-20s\", i is %-10d and x is %-10f\n", s, i, x);

  // using max field width
  printf("\nUsing max field width:\n");
  printf("s is \"%.2s\", i is %.2d and x is %.2f\n", s, i, x);
  // using max field to extend printed decimals
  printf("\nFull value of x is %.10f\n", x);
  
  return EXIT_SUCCESS;
}
