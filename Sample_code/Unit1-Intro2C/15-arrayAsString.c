// Treating character arrays as strings
// Deitel & Deitel, C How to Program
//
// 12/09/2018

#include <stdio.h>
#include <stdlib.h>
#define SIZE 20

int main()
{
  char string1[20]; // reserve 20 characters
  char string2[] = "string literal"; // reserve 15 characters
                                     // 14 for literal + null character
  size_t i; //counter

  // read string from user into array string1
  // limit is 19 char, as null char will be appended!
  printf("%s", "Enter a string (no longer than 19 characters): ");
  scanf("%19s", string1); // input no more than 19 chars

  // output strings
  printf("string1 is: %s\nstring2 is %s\n"
	 "string1 with spaces between characters is:\n",
	 string1, string2);

  // output characters until null character is reached
  for (i=0; i<20 && string1[i] != '\0'; ++i) {
    printf("%c ", string1[i]);
  }

  puts("");

  return EXIT_SUCCESS;
}
