/*
 * 17-pointers.c 
 * Test pointers and get an understanding for them 
 *
 * author:	Andy Hinkel
 * date:	06.10.2022
 */


#include <stdio.h>
int main(){
	// some comments to begin with:
	// - IGNORE THE WARNINGS WHEN COMPILING THIS FILE - it works fine
	//
	// - use 
	//    gcc -o ptrTst pointerTest.c
	//   ./ptrTst
	//
	// - usually cpp and c compilers are used interchangeably, this might be confusing at first (you'll get used to it) 
	//
	// - PRINTING VARIABLES IN C IS A LITTLE CRAZY at first. the ... in  %... will indicate how to format something properly. 
	//   See https://cplusplus.com/reference/cstdio/printf/ for details.

	printf("Each variable has 2 fields: \n- ADDRESS(hex) and \n- VALUE\n");
	printf("POINTERS are variables\nwhich have a value that is an address of another value\n\n");
	//But when to use *,**,&,... ?
	// int * pX 	- * modify the type to be a pointer
	// int *pX 	- * modify the type to be a pointer
	// int* pX 	- * modify the type to be a pointer
	// *pX 		- the value pX points to == value of x
	// &x 		- the address of x
	
	// set ordinary integer x to value 4 (you won't care about its address usually, the OS will find free space in your memory)
	int x = 4;
	printf("SET NORMAL VARIABLE		int x\n");
	printf("				int x = %i\n",x);
	// EVERY VARIABLE has two fields : address and value. & = address
	printf("ADDRESS OF X 			&x = %#08x\n\n",&x);

	//declaring pointer *pX and initialize its value to the address of x. 
	// *pX points therefore to what is stored at this address which is THE VALUE of X
	// ACCEPT THIS - DON'T PONDER - JUST ACCEPT
	int *pX= &x;
	printf("SET POINTER 			*pX \n");
	printf("				int *pX = &x\n\n");
	printf("VALUE OF X			*pX = %d\n",*pX);

	//when we ignore the * we will get the actual value of pX which is the ADDRESS OF X.
	printf("ADDRESS of X 			pX = %#08x\n\n",pX);
	// using the & we get the actual  ADDRESS OF THE POINTER itself
	printf("ADDRESS OF pX			&pX = %#08x\n\n",&pX);
	
	// so what happens when we ALTER THE VALUE *pX gives us?
	*pX=8;
	printf("CHANGE X VALUE\n");
	printf("VIA POINTER			*pX = %d\n\n",*pX);

	// it actually CHANGES THE VALUE OF X!!!
	printf("PROOF THAT YOU JUST ALTERED X\n");
	printf("VALUE OF X			x = %d\n\n",x);
	// but why? well as *pX==value of x, we work on top of the x value.
	//
	// So pointers are crazy but powerful. 
	// 
	// But What do we need them for? We work closer on the system level, less abstract than higher programming languages like python. 
	// they use them as well but you don't see them. 
	// It is very useful to understand the concept of pointers as they are used in the OS all the time.
	//
	// for example when you call a function func(int * a,int *  b)
	// when you hand over pointers the values won't get copied somewhere else but it will be operated directly on top of the values they point to. i
	// Therefore, not everything will be forgotten once the function finished. 
	//
	// Mechanisms to allocate memory dynamically 
	// i.e. at runtime==dynamically==on demand==not hard coded
	// e.g if you require user input you allocate memory depending on the size of the user input =not hard coded. 
	//
	// One method for doing this is malloc() which stands for memory allocation ) 
	// it requires to physically split up data. E.g. you have a list of int and you extend it. 
	// IT MIGHT WON'T BE PHYSICALLY NEXT TO EACH OTHER IN THE MEMORY BUT
	// IT WILL FEEL NEXT TO EACH OTHER TO THE USER as pointer abstract this to us users. But when we want to work closer on OS level, we will come across them all the time. 
	// This is because C is closer to hardware and therefore less abstraction. 
	//
	// Just one more hint : you might come across segmentation faults, just google them. You're trying to access memory that you did not allocate before therefore you do not have reading permission.
	printf("You'll do fine, it is a matter of practice.\n\nDon't panic. :)\n\nJust practice. ;)\n");
	
	return 0;
}

