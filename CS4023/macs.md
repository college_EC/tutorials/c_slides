# Apple M chip Macs
## Lab 1
Something that has come up in the completion of the introduction part to this lab assignment is that Apple M chip macs is
that those chips automatically compiles char variables as unsigned, as such the code provided: <br />
```C
int main(){
    char c;
    FILE *from;
    
    from = fopen("file.txt", "r");
    if (from == NULL){
        perror("file.txt");
        exit(1);
    }
    
    while ((c = getc(from)) != EOF){
        putc(c, stdout);
    }
    fclose(from);
    exit(0);
}
```
Which was supposed to print out the contents of the file called ``file.txt``, and if it was run on an x86 or AMD processor 
it would do just that but unfortunately the Apple M series processor it compiles the program as if you'd written as this:
```C
    unsigned char c;
```
which meant that it was printing out � repeatedly. The fix was to instead of just declaring as a char but to declare it as:
```C
    signed char c;
```
which forces the processor and compiler to work the way that it is expected.