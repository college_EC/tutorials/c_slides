// Displaying a histrogram
// Deitel & Deitel, C How to Program
//
// 12/09/2018

#include <stdlib.h>
#include <stdio.h>

// function main begins program execution
int main()
{
  // use initializer list to init array n
  int elemPerRange[] = {19, 3, 15, 7, 11};
  char *ranges[] = {"0-9", "10-19", "20-29", "30-39", "40-49"};

  // print header
  printf("%7s%13s%16s\n", "Range", "Elements", "Histogram");

  // for each element of array elemPerRange, output a bar of the histrogram
  for (unsigned i=0; i<5; ++i) {
    printf("%7s%13d       ", ranges[i], elemPerRange[i]);
    for (int j=1; j<=elemPerRange[i]; ++j) { // print one bar
      printf("%c", '*');
    }
    puts(""); // end histogram with newline
  }
  return EXIT_SUCCESS;
}
