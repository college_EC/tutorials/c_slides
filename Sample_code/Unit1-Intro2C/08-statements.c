/************************************************************
 * statements.c
 * sample program to introduce basic statements in the 
 * c programming language.
 *
 * author:	reiner dojen
 * date:	05.09.2007
 ************************************************************/

#include <stdlib.h>
#include <stdio.h>

void ifElseStmt();
void switchStmtBreak();
void whileLoop();
void doWhileLoop();
void forLoop();
void continueStmt();

int main() 
{
  // declare/define variables 
  int a = 1;
  int b = 2;
  int c = 3;
  
  // empty statement - doing nothing
  ;
  
  
  // expression statement
  a; // legal, but with no effect
  5; // legal, but with no effect
  a = 5;
  b = 6;
  a + b; // legal, but with no effect
  c = a * b;
    
  // compound statement
  {
    a = b;
    b = a+c;
    // int tmp = 6;
  }
  // a = tmp; // illegal - tmp is here out of scope

  ifElseStmt();
  switchStmtBreak();
  whileLoop();
  doWhileLoop();
  forLoop();
  continueStmt();
  
  return EXIT_SUCCESS;
}

// demonstrates use of if - else if - else consruct
void ifElseStmt()
{
  int a=6;
  int b=5;
  int c=a;
  
  // if-else-if statement
  // Note: for single line if/else clauses curly brackets are
  // not required. 
  // I recommend to *always* use curly brackets
  if (a > 5) {
    printf("a is greater than 5\n");
  } else if (b < 0) {
    printf("b is negative\n");
  } else if (c == a) {
    printf("c equals a\n");
    printf("use compound statement for 'complex' cases\n");
  } else {
    printf("none of above statements is true\n");
  }
}

// demonstrates switch statement with break
void switchStmtBreak()
{
  int a=6;
  
  // switch statement
  // cases can appear in any order
  // where possible numeric order recommended
  switch (a) {
  case 0:
    printf(" a equals 0\n");
    break;
  case 1:
    printf(" a equals 1\n");
    break;
  case 2:
    printf(" a equals 2\n");
    break;
    // cases can be combined
  case 5:
  case 10:
    printf(" a equals 5 or 10\n");
    break;
  default:
    printf("none of above cases applies\n");
  }
}

// demonstrates use of while loop
void whileLoop()
{  
  // while loop (perform a task until a condition applies)
  int a = 10;
  // loop as long as a is greater 0
  while (a > 0) {
    printf("(while loop) a = %d\n",a );
    a = a-1;
  }  
}

// demonstrates use of do-while loop
void doWhileLoop()
{  
  // do-while loop (perform a taks at least once until a condition applies)
  int a = -5;
  // loop as long as a is greater 0
  do {
    printf("(do-while loop) a = %d\n",a);
    a = a-1;
  } while (a > 0);
}

// demonstrate use of for loop
void forLoop()
{
  // for loop (perform a task a fixed number of times)
  for (int a=0; a<10; a++) {
    printf("(for loop) a = %d\n", a);
  }
}

// demonstrates use of continue statement
void continueStmt()
{
  // continue statement
  for (int a=-5; a<=5; a++) {
    // ignore case a=0;
    if (a != 0) {
      printf("1/%d = %lf\n", a, 1.0/a);
      printf("looping with a = %d\n", a);
    }
  }
}

