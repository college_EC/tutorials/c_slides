/*
 * ArrayDataMain.cpp
 * Test main function for ArrayData object implementation.
 *
 * author:	reiner dojen
 * date:	20.09.2012
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h> //used for randomizing rand()

// declare all functions
void initialiseData(int data[10][12][31], int value);
void fillRandomData(int data[10][12][31]);
double getAverage(int data[10][12][31]);
double getYearlyAverage(int data[10][12][31], int year);
double getMonthlyAverage(int data[10][12][31], int year, int month);
void printMonthlyData(int data[10][12][31], int year, int month);

int main() {
  
  // create 3-dimensional array
  int data [10][12][31];
  
  // initialise to zero
  initialiseData(data, 0);
  // print zero values
  printf("\nprint zero values: \n");
  printMonthlyData(data, 0, 0);

  // initialise random generator and fill
  // data with random values
  srand(time(NULL));
  fillRandomData(data);

  // print random values
  printf("\nprint random values: \n");
  printMonthlyData(data, 0, 0);
  
  // get some average values
  printf("\nOverall Average = %f\n", getAverage(data));
  printf("Average of first year = %f\n",  getYearlyAverage(data, 0));
  
  return EXIT_SUCCESS;
}

/*
 * fill data collection with given value
 * 
 * parameter:
 * data[][][] - data for 10 years of 12 month of 31 days
 * value - value to be stored
 * return: void
 */
void initialiseData(int data[10][12][31], int value) {
  for (int y=0; y<10; ++y)
    for (int m=0; m<12; ++m)
      for (int d=0; d<31; ++d)
	data[y][m][d] = value;
}

/*
 * fill data collection with random values in range 0 to 100 (inclusive)
 * 
 * parameter:
 * data[][][] - data for 10 years of 12 month of 31 days
 * return: void
 */
void fillRandomData(int data[10][12][31]) {
  for (int y=0; y<10; ++y)
    for (int m=0; m<12; ++m)
      for (int d=0; d<31; ++d)
	data[y][m][d] = rand()%101;
}

/* 
 * get average of all values
 *
 * parameter:
 * data[][][] - data collection
 * return: average of all values
 */
double getAverage(int data[10][12][31]) {
  double average = 0; // result for average value
  for (int year=0; year<10; ++year) {
    average += getYearlyAverage(data, year);
  }
  average /= 10.0;
  return average;
}

/* 
 * get average value of a particular year
 *
 * parameter:
 * data[][][] - data collection
 * year - year for which average is required
 * return: average value of indicated year
 */
double getYearlyAverage(int data[10][12][31], int year) {
  double average = 0; // result for average value
  for (int month=0; month<12; ++month) {
    average += getMonthlyAverage(data, year, month);
  }
  average /= 12.0;
  return average;
}
  
/* 
 * get average value of a particular year/month
 *
 * parameter:
 * data[][][] - data collection
 * year - year for month whose average is required
 * month - month of the year for which average is required
 * return: average value of indicated year/month
 */
double getMonthlyAverage(int data[10][12][31], int year, int month) {
  double average = 0; // result for average value
  int maxDays=0; // max number of days for year/month

  // establish max number of days
  switch (month) {
  case 0: // January
  case 2: // March
  case 4: // May
  case 6: // July
  case 7: // August
  case 9: // October
  case 11: // December
    maxDays = 31;
    break;

  case 3: // April
  case 5: // June
  case 8: // September
  case 10: // November
    maxDays = 30;
    break;
  case 1: // February 
    // is is leap year? - use incomplete remainder of 4 approach
    if (year%4 == 0) {
	maxDays = 29;
      } else {
	maxDays = 28;
      }
      break;
    }
    for (int day=0; day<maxDays; ++day) {
      average += data[year][month][day];
    }
    average /= maxDays;
    return average;
}

/* 
 * prints the daily data for a given month of a given year to stdout
 * 
 * parameter:
 * data[][][] - data collection
 * year - year for month whose average is required
 * month - month of the year for which average is required
 * return - void
 */
void printMonthlyData(int data[10][12][31], int year, int month) {
  int maxDays=0; // max number of days for year/month
  
  // establish max number of days
  switch (month) {
  case 0: // January
  case 2: // March
  case 4: // May
  case 6: // July
  case 7: // August
  case 9: // October
  case 11: // December
    maxDays = 31;
    break;
  case 3: // April
  case 5: // June
  case 8: // September
  case 10: // November
    maxDays = 30;
    break;
  case 1: // February
    // is is leap year? - use incomplete remainder of 4 approach
    if (year%4 == 0) {
      maxDays = 29;
    } else {
      maxDays = 28;
    }
    break;
  }
  printf("Data for %dth month of year %d\n", month, year);
  for (int day=1; day<maxDays; ++day) {
    // attention: days run from 1 to maxDays-1,
    //            indices run from 0 to maxDays-2 !!!!
    printf("day %d: %d", day, data[year][month][day-1]);
    // pretty print 4 values per row
    if (day == maxDays-1) printf("\n");
    else if ((day%4 != 0) || (day==0) ) printf(",\t");
    else printf("\n");
  }
}

